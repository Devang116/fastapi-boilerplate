# FastAPI Docker boilerplate

This is a FastAPI application set up to run inside a Docker container.

## Build and run the application with Docker Compose

```bash
docker-compose up -d
